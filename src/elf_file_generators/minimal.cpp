#include <iostream>

#include <elf.h>

#include "../parts/elf_header.cpp"
#include "../parts/program_header.cpp"
#include "../parts/text_section.cpp"
#include "../parts/padding.cpp"

#define TEXT_SEGMENT_FILE_OFFSET 0x40 * 2
#define MEMORY_OFFSET 0x80 // the offset for the .text segment in process memory

void generate_minimal(FILE* output_file) {
	Elf64_Addr execution_entry_point = 0x400000 + MEMORY_OFFSET;
	Elf64_Off program_table_file_offset = ELF_HEADER_SIZE;

	elf_assembling::elf_header_assembler elf_header = 
	elf_assembling::elf_header_assembler(
		execution_entry_point, // initial pointer to text segment
		new elf_assembling::header_table_information(program_table_file_offset, 0x1), // program table
		new elf_assembling::header_table_information(0x0, 0x0),  // section table
		0x0 // string related
	);

	elf_assembling::program_header_assembler program_header = 
	elf_assembling::program_header_assembler(
		PT_LOAD, // what kind of segment
		TEXT_SEGMENT_FILE_OFFSET, // the offset of the segment in memory
		execution_entry_point, // the offset im memory of where the first byte of the segment resides
		TEXT_SEGMENT_SIZE, // size of the segment in file image
		TEXT_SEGMENT_SIZE, // size of the segment in memory
		PF_X | PF_R, // read and execute flags
		0x80 // the alignemt of the section
	);

	// section header
	fwrite((char*)elf_header.assemble_header(), sizeof(uint8_t), ELF_HEADER_SIZE, output_file);
	// program header
	fwrite((char*)program_header.assemble_header(), sizeof(uint8_t), PROGRAM_HEADER_SIZE, output_file);
	fwrite((char*)elf_assembling::padding_assembler().assemble_padding(8), sizeof(uint8_t), 8, output_file); // WARNING: Assumes that the program header table is less than 0x40 bytes long!
	// assemble the text section
	fwrite((char*)elf_assembling::text_section_assembler().assemble_section(), sizeof(uint8_t), TEXT_SEGMENT_SIZE, output_file);
	fwrite((char*)elf_assembling::padding_assembler().assemble_padding(GENERIC_SIZE - TEXT_SEGMENT_SIZE), sizeof(uint8_t), GENERIC_SIZE - TEXT_SEGMENT_SIZE, output_file); // WARNING: Assumes that the text segment is less than 0x40 bytes long!
}

#undef TEXT_SEGMENT_FILE_OFFSET
#undef MEMORY_OFFSET