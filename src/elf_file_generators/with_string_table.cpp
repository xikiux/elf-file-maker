#include <iostream>

#include <elf.h>

#include "../parts/elf_header.cpp"
#include "../parts/program_header.cpp"
#include "../parts/text_section.cpp"
#include "../parts/padding.cpp"
#include "../parts/section_header.cpp"
#include "../parts/string_table.cpp"

#define TEXT_SEGMENT_FILE_OFFSET (0x40 * 6)
#define MEMORY_OFFSET (0x40 * 6) // the offset for the .text segment in process memory

void generate_with_string_table(FILE* output_file) {
	Elf64_Addr execution_entry_point = 0x400000 + MEMORY_OFFSET;
	Elf64_Off program_table_file_offset = ELF_HEADER_SIZE;
	Elf64_Off section_table_file_offset = ELF_HEADER_SIZE + PROGRAM_HEADER_SIZE + 8;

	// elf headers pre generation
	elf_assembling::elf_header_assembler elf_header = 
	elf_assembling::elf_header_assembler(
		execution_entry_point, // initial pointer to text segment
		new elf_assembling::header_table_information(program_table_file_offset, 0x1), // program table
		new elf_assembling::header_table_information(section_table_file_offset, 0x3), // section table
		0x1 // string related
	);

	elf_assembling::program_header_assembler program_header = 
	elf_assembling::program_header_assembler(
		PT_LOAD, // what kind of segment
		TEXT_SEGMENT_FILE_OFFSET, // the offset of the segment in memory
		execution_entry_point, // the offset im memory of where the first byte of the segment resides
		TEXT_SEGMENT_SIZE, // size of the segment in file image
		TEXT_SEGMENT_SIZE, // size of the segment in memory
		PF_X | PF_R, // read and execute flags
		MEMORY_OFFSET // the alignemt of the section
	);

	elf_assembling::section_header_assembler blank_section_header = 
	elf_assembling::section_header_assembler(
		0x0,
		0x0,
		0x0,
		0x0,
		0x0,
		0x0,
		0x0,
		0x0,
		0x0,
		0x0
	);

	elf_assembling::section_header_assembler string_table_section_header = 
	elf_assembling::section_header_assembler(
		0x0,
		SHT_STRTAB,
		0x0, // section is not executable or loadable
		0x0,
		GENERIC_SIZE * 5,
		STRING_TABLE_SIZE,
		0x0,
		0x0,
		0x0, // might be important
		0x0
	);

	elf_assembling::section_header_assembler text_section_header = 
	elf_assembling::section_header_assembler(
		0x1,
		SHT_PROGBITS,
		SHF_ALLOC | SHF_EXECINSTR,
		MEMORY_OFFSET,
		TEXT_SEGMENT_FILE_OFFSET,
		TEXT_SEGMENT_SIZE,
		0x0,
		0x0,
		MEMORY_OFFSET,
		0x0
	);

	

	// elf gereration
	// section header
	fwrite((char*)elf_header.assemble_header(), sizeof(uint8_t), ELF_HEADER_SIZE, output_file);
	// program header
	fwrite((char*)program_header.assemble_header(), sizeof(uint8_t), PROGRAM_HEADER_SIZE, output_file);
	fwrite((char*)elf_assembling::padding_assembler().assemble_padding(GENERIC_SIZE - PROGRAM_HEADER_SIZE), sizeof(uint8_t), GENERIC_SIZE - PROGRAM_HEADER_SIZE, output_file); // WARNING: Assumes that the program header table is less than 0x40 bytes long!
	// initial section header
	fwrite((char*)blank_section_header.assemble_header(), sizeof(uint8_t), SECTION_HEADER_SIZE, output_file);
	//  string table section header
	fwrite((char*)string_table_section_header.assemble_header(), sizeof(uint8_t), SECTION_HEADER_SIZE, output_file);
	// .text section header
	fwrite((char*)text_section_header.assemble_header(), sizeof(uint8_t), SECTION_HEADER_SIZE, output_file);
	// assemble the string table
	fwrite((char*)elf_assembling::string_table_assembler().assemble_section(), sizeof(uint8_t), STRING_TABLE_SIZE, output_file);
	fwrite((char*)elf_assembling::padding_assembler().assemble_padding(GENERIC_SIZE - STRING_TABLE_SIZE), sizeof(uint8_t), GENERIC_SIZE - STRING_TABLE_SIZE, output_file); // WARNING: Assumes that the text segment is less than 0x40 bytes long!
	// assemble the text section
	fwrite((char*)elf_assembling::text_section_assembler().assemble_section(), sizeof(uint8_t), TEXT_SEGMENT_SIZE, output_file);
	fwrite((char*)elf_assembling::padding_assembler().assemble_padding(GENERIC_SIZE - TEXT_SEGMENT_SIZE), sizeof(uint8_t), GENERIC_SIZE - TEXT_SEGMENT_SIZE, output_file); // WARNING: Assumes that the text segment is less than 0x40 bytes long!
}

#undef TEXT_SEGMENT_FILE_OFFSET
#undef MEMORY_OFFSET