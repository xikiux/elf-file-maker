#pragma once

#include <cstring>

#include <elf.h>

#include "../sizes.hpp"

namespace elf_assembling {
	class header_table_information {
	public:
		Elf64_Off table_offset;
		Elf64_Half entry_count;

		header_table_information(Elf64_Off _table_offset, Elf64_Half _entry_count) {
			table_offset = _table_offset;
			entry_count = _entry_count;
		}
	};

	class elf_header_assembler {
		Elf64_Ehdr* header;

	public:
		elf_header_assembler(
			Elf64_Addr execution_entry_point,

			header_table_information* program_table,
			header_table_information* section_table,

			Elf64_Half section_header_entry_index_number_of_string_table
		) {
			header = new Elf64_Ehdr();

			// create the elf magic number
			header->e_ident[0] = 0x7f;
			header->e_ident[1] = 'E';
			header->e_ident[2] = 'L';
			header->e_ident[3] = 'F';
			header->e_ident[4] = ELFCLASS64;
			header->e_ident[5] = ELFDATA2LSB;
			header->e_ident[6] = EV_CURRENT;
			header->e_ident[7] = ELFOSABI_SYSV;
			header->e_ident[8] = 0;
			header->e_ident[9] = 0;
			header->e_ident[10] = 0;
			header->e_ident[11] = 0;
			header->e_ident[12] = 0;
			header->e_ident[13] = 0;
			header->e_ident[14] = 0;
			header->e_ident[15] = 0;

			header->e_type = ET_EXEC;
			header->e_machine = EM_X86_64;
			header->e_version = EV_NONE;
			header->e_entry = execution_entry_point;
			
			header->e_phoff = program_table->table_offset;
			header->e_shoff = section_table->table_offset;

			header->e_flags = 0x0;
			header->e_ehsize = ELF_HEADER_SIZE;
			header->e_phentsize = PROGRAM_HEADER_SIZE;
			header->e_phnum = program_table->entry_count;
			header->e_shentsize = SECTION_HEADER_SIZE;
			header->e_shnum = section_table->entry_count;
			header->e_shstrndx = section_header_entry_index_number_of_string_table;
		}

		uint8_t* assemble_header() {
			uint8_t* bytes = new uint8_t[ELF_HEADER_SIZE];
			int index = 0;

			write_to_buffer(bytes, &header->e_ident, 16, &index);
			write_to_buffer(bytes, &header->e_type, sizeof(header->e_type), &index);
			write_to_buffer(bytes, &header->e_machine, sizeof(header->e_machine), &index);
			write_to_buffer(bytes, &header->e_version, sizeof(header->e_version), &index);
			write_to_buffer(bytes, &header->e_entry, sizeof(header->e_entry), &index);
			write_to_buffer(bytes, &header->e_phoff, sizeof(header->e_phoff), &index);
			write_to_buffer(bytes, &header->e_shoff, sizeof(header->e_shoff), &index);
			write_to_buffer(bytes, &header->e_flags, sizeof(header->e_flags), &index);
			write_to_buffer(bytes, &header->e_ehsize, sizeof(header->e_ehsize), &index);
			write_to_buffer(bytes, &header->e_phentsize, sizeof(header->e_phentsize), &index);
			write_to_buffer(bytes, &header->e_phnum, sizeof(header->e_phnum), &index);
			write_to_buffer(bytes, &header->e_shentsize, sizeof(header->e_shentsize), &index);
			write_to_buffer(bytes, &header->e_shnum, sizeof(header->e_shnum), &index);
			write_to_buffer(bytes, &header->e_shstrndx, sizeof(header->e_shstrndx), &index);

			return bytes;
		}
	private:
		void write_to_buffer(uint8_t* array, const void* src, size_t object_size, int* increment) {
			memcpy((void*)((array) + (*increment)), src, object_size);
			(*increment) += object_size;
		}
	};
}