#pragma once

#include <cstring>

#include <elf.h>

#include "../sizes.hpp"

namespace elf_assembling {
	class section_header_assembler {
		Elf64_Shdr* header;

	public:
		section_header_assembler(
			Elf64_Word section_string_table_name_index,
			Elf64_Word section_type,
			Elf64_Xword section_flags,
			Elf64_Addr section_in_memory_address,
			Elf64_Off section_file_byte_offset,
			Elf64_Xword section_in_file_size,
			Elf64_Word section_link,
			Elf64_Word section_info,
			Elf64_Xword section_alignment,
			Elf64_Xword section_fixed_entry_size
		) {
			header = new Elf64_Shdr();

			header->sh_name = section_string_table_name_index;
			header->sh_type = section_type;
			header->sh_flags = section_flags;
			header->sh_addr = section_in_memory_address;
			header->sh_offset = section_file_byte_offset;
			header->sh_size = section_in_file_size;
			header->sh_link = section_link;
			header->sh_info = section_info;
			header->sh_addralign = section_alignment;
			header->sh_entsize = section_fixed_entry_size;
		}

		uint8_t* assemble_header() {
			uint8_t* bytes = new uint8_t[SECTION_HEADER_SIZE];
			int index = 0;

			write_to_buffer(bytes, &header->sh_name, sizeof(header->sh_name), &index);
			write_to_buffer(bytes, &header->sh_type, sizeof(header->sh_type), &index);
			write_to_buffer(bytes, &header->sh_flags, sizeof(header->sh_flags), &index);
			write_to_buffer(bytes, &header->sh_addr, sizeof(header->sh_addr), &index);
			write_to_buffer(bytes, &header->sh_offset, sizeof(header->sh_offset), &index);
			write_to_buffer(bytes, &header->sh_size, sizeof(header->sh_size), &index);
			write_to_buffer(bytes, &header->sh_link, sizeof(header->sh_link), &index);
			write_to_buffer(bytes, &header->sh_info, sizeof(header->sh_info), &index);
			write_to_buffer(bytes, &header->sh_addralign, sizeof(header->sh_addralign), &index);
			write_to_buffer(bytes, &header->sh_entsize, sizeof(header->sh_entsize), &index);

			return bytes;
		}
	private:
		void write_to_buffer(uint8_t* array, const void* src, size_t object_size, int* increment) {
			memcpy((void*)((array) + (*increment)), src, object_size);
			(*increment) += object_size;
		}
	};
}