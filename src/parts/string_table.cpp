#pragma once

#include <iostream>
#include <cstring>

#include <elf.h>

#include "../sizes.hpp"

namespace elf_assembling {
	class string_table_assembler {
	public:
		uint8_t* assemble_section() {
			unsigned char section[] = { // using this instead of a string to avoid alignment confusion with strings adding an invisible zero
				'\0',
				'.', 't', 'e', 'x', 't', '\0',
				'.', 'r', 'o', 'd', 'a', 't', 'a', '\0',
				'.', 's', 'h', 's', 't', 'r', 't', 'a', 'b', '\0' 
			 };
			uint8_t* bytes = new uint8_t[sizeof(section)];

			for (int i = 0; i < sizeof(section); i++) {
				bytes[i] = section[i];
			}

			return bytes;
		}
	private:
		void write_to_buffer(uint8_t* array, const void* src, size_t object_size, int* increment) {
			memcpy((void*)((array) + (*increment)), src, object_size);
			(*increment) += object_size;
		}
	};
}