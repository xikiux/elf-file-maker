#pragma once

#include <iostream>
#include <cstring>

#include <elf.h>

#include "../sizes.hpp"

namespace elf_assembling {
	class program_header_assembler {
		Elf64_Phdr* header;

	public:
		program_header_assembler(
			Elf64_Word segment_type,
			Elf64_Off file_offset,
			Elf64_Addr virtual_memory_address,
			Elf64_Xword size_of_section_on_file,
			Elf64_Xword size_of_section_in_memory,
			Elf64_Word section_flags,
			Elf64_Xword segment_alignment
		) {
			header = new Elf64_Phdr();

			header->p_type = segment_type;
			header->p_offset = file_offset;
			header->p_vaddr = virtual_memory_address;
			header->p_paddr = 0;
			header->p_filesz = size_of_section_on_file;
			header->p_memsz = size_of_section_in_memory;
			header->p_flags = section_flags;
			header->p_align = segment_alignment; // both alignments in memory and on file!!!
		}

		uint8_t* assemble_header() {
			uint8_t* bytes = new uint8_t[PROGRAM_HEADER_SIZE];
			int index = 0;

			write_to_buffer(bytes, &header->p_type, sizeof(header->p_type), &index);
			write_to_buffer(bytes, &header->p_flags, sizeof(header->p_flags), &index);
			write_to_buffer(bytes, &header->p_offset, sizeof(header->p_offset), &index);
			write_to_buffer(bytes, &header->p_vaddr, sizeof(header->p_vaddr), &index);
			write_to_buffer(bytes, &header->p_paddr, sizeof(header->p_paddr), &index);
			write_to_buffer(bytes, &header->p_filesz, sizeof(header->p_filesz), &index);
			write_to_buffer(bytes, &header->p_memsz, sizeof(header->p_memsz), &index);
			write_to_buffer(bytes, &header->p_align, sizeof(header->p_align), &index);

			return bytes;
		}
	private:
		void write_to_buffer(uint8_t* array, const void* src, size_t object_size, int* increment) {
			memcpy((void*)((array) + (*increment)), src, object_size);
			(*increment) += object_size;
		}
	};
}