#pragma once

#include <iostream>
#include <cstring>

#include <elf.h>

#include "../sizes.hpp"

namespace elf_assembling { 
	class padding_assembler {
	public:
		uint8_t* assemble_padding(int byte_length) {
			uint8_t* bytes = new uint8_t[byte_length];
			uint8_t* filler = new uint8_t[1];
			int index = 0;

			filler[0] = 0x00;

			for (int i = 0; i < byte_length; i++) {
				write_to_buffer(bytes, filler, 1, &index);
			}

			return bytes;
		}
	private:
		void write_to_buffer(uint8_t* array, const void* src, size_t object_size, int* increment) {
			memcpy((void*)((array) + (*increment)), src, object_size);
			(*increment) += object_size;
		}
	};
}