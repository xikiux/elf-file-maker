#pragma once

#define GENERIC_SIZE 0x40 // this is the standard size of each segment (if smaller then blank data fills in)

#define ELF_HEADER_SIZE 0x40
#define PROGRAM_HEADER_SIZE 0x38
#define SECTION_HEADER_SIZE 0x40

#define TEXT_SEGMENT_SIZE 39
#define STRING_TABLE_SIZE 25
#define RODATA_SIZE 13