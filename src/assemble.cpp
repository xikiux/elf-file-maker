#include <iostream>

#include "elf_file_generators/minimal.cpp"
#include "elf_file_generators/with_section_headers.cpp"
#include "elf_file_generators/with_string_table.cpp"
#include "elf_file_generators/with_hello_world.cpp"

int main() {
	FILE* output_file = fopen("exe/program", "wb+");

	//generate_minimal(output_file);
	//generate_with_section_headers(output_file);
	//generate_with_string_table(output_file); // might have accidentally completely destroyed this
	generate_with_hello_world(output_file);

	fclose(output_file);
}